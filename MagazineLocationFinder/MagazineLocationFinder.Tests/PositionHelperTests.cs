﻿
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MagazineLocationFinder.Tests
{
    [TestClass]
    public class PositionHelperTests
    {
        [DataTestMethod]
        [DataRow(false, 6, -1)]
        [DataRow(false, 4, 5)]
        [DataRow(false, 5, -1)]
        [DataRow(true, 5, 10)]
        public void FindFreePlace_WorksForAboveParametersCorrectly(bool isRotary , int neededPlaces , int expectedAmount)
        {
            int result = PositionHelper.FindFreePlace(new bool[] { false, false, false, true, true, false, false, false, false, true, false, false },
                isRotary, 
                neededPlaces);
            Assert.AreEqual(expectedAmount, result);
        }
    }
}
