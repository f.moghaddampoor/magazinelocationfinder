﻿namespace MagazineLocationFinder
{
    /// <summary>
    /// Helps find position for needed places
    /// </summary>
    public static class PositionHelper
    {
        /// <summary> 
        /// Function to return the free place in the magazine level
        /// </summary>
        /// <param name="places">Array of bools of dimension "n". True means occupied position, false means available</param> 
        /// <param name="isRotary">Flag whether the level is rotary (last position is neighbour of the first one)</param>
        /// <param name="neededPlaces">Number of places needed</param> 
        /// <returns>Index of first position found (zero based) or -1 if no position is found</returns> 
        public static int FindFreePlace(bool[] places, bool isRotary, int neededPlaces)
        {
            int result = -1;
            int limit = places.Length;
            for (int placeIndex = 0; placeIndex < places.Length; placeIndex++)
            {
                int AvailablePositionAtIndex = 0;
                for (int placeIndex1 = placeIndex; placeIndex1 < limit; placeIndex1++)
                {
                    if (!places[placeIndex1])
                    {
                        AvailablePositionAtIndex++;
                    }
                    else
                    {
                        break;
                    }
                    if ((placeIndex1 == places.Length - 1) && isRotary)
                    {
                        limit = placeIndex;
                        placeIndex1 = -1;
                    }
                }
                if (neededPlaces <= AvailablePositionAtIndex) 
                {
                    result = placeIndex;
                    break;
                }
            }
            return result;
        }
    }
}
