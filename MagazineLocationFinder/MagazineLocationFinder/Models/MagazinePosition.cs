﻿namespace MagazineLocationFinder.Models
{
    /// <summary>
    /// Magazine position defenition
    /// </summary>
    public class MagazinePosition
    {
        /// <summary>
        /// The position number
        /// </summary>
        public int Position { get; set; }

        /// <summary>
        /// True if position is occupied, false if position is not occupied
        /// </summary>
        public bool Occupied { get; set; }
    }
}
