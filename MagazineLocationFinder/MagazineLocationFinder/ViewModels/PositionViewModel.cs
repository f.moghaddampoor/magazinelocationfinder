﻿using MagazineLocationFinder.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;

namespace MagazineLocationFinder.ViewModels
{
    public class PositionViewModel: INotifyPropertyChanged
    {
        private const string _magazineDBFileName = "Magazine.csv";
        private const string _magazineSeparator = ",";
        private ObservableCollection<MagazinePosition> _magazinePositions;
        public ObservableCollection<MagazinePosition> MagazinePositions
        {
            get
            {
                return _magazinePositions;
            }
            set
            {
                _magazinePositions = value;
                RaisePropertyChanged("MagazinePositions");
            }
        }
        public PositionViewModel()
        {
            MagazinePositions = new ObservableCollection<MagazinePosition>();
            LoadMagazine();
        }

        private void LoadMagazine()
        {
            using var reader = new StreamReader(_magazineDBFileName);
            int lineIndex = -1;
            while (!reader.EndOfStream)
            {
                lineIndex++;
                var line = reader.ReadLine();
                var values = line.Split(_magazineSeparator);
                bool occupied = values[1] == "1";
                if (int.TryParse(values[0], out int position))
                {
                    MagazinePosition magazinePosition = new MagazinePosition()
                    {
                        Position = position,
                        Occupied = occupied
                    };
                    MagazinePositions.Add(magazinePosition);
                }
                else
                {
                    Console.WriteLine($"Invalid position at line {lineIndex}");
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
    }
}
