﻿using MagazineLocationFinder.ViewModels;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace MagazineLocationFinder
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Number of places needed
        /// </summary>
        private int numberOfPlacesNeeded = 1;

        /// <summary>
        /// Position view model
        /// </summary>
        private readonly PositionViewModel _positionViewModel;

        /// <summary>
        /// Number of places needed property
        /// </summary>
        public int NumberOfPlacesNeeded
        {
            get { return numberOfPlacesNeeded; }
            set
            {
                numberOfPlacesNeeded = value;
                txtNumberOfPlacesNeeded.Text = value.ToString();
            }
        }

        /// <summary>
        /// Initialize window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Create the ViewModel to which the main window binds.
            _positionViewModel = new PositionViewModel();

            // Allow all controls in the window to 
            // bind to the ViewModel by setting the 
            // DataContext, which propagates down 
            // the element tree.
            this.DataContext = _positionViewModel;

            txtNumberOfPlacesNeeded.Text = numberOfPlacesNeeded.ToString();
        }

        /// <summary>
        /// Upon loading windows it will be called
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            // Method intentionally left empty.
        }

        /// <summary>
        /// Upon clicking the close menu will be called
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void MenuItemClose_Click(object sender, RoutedEventArgs e)
        {
            //Closes the Application
            this.Close();
        }

        /// <summary>
        /// Adds one to the number of places needed
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void AddOneToNumberOfPlacesNeeded(object sender, RoutedEventArgs e)
        {
            NumberOfPlacesNeeded++;
        }

        /// <summary>
        /// Reduce one from the number of places needed
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void ReduceOneFromNumberOfPlacesNeeded(object sender, RoutedEventArgs e)
        {
            if (NumberOfPlacesNeeded - 1 > 0)
            {
                NumberOfPlacesNeeded--;
            }  
        }

        /// <summary>
        /// Over changes in number of places needed will be called
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void TxtNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            //In case the text is empty skip
            if (txtNumberOfPlacesNeeded == null)
            {
                return;
            }

            //Control text is a number and set the text
            if (!int.TryParse(txtNumberOfPlacesNeeded.Text, out numberOfPlacesNeeded))
            {
                txtNumberOfPlacesNeeded.Text = numberOfPlacesNeeded.ToString();
            }
        }

        /// <summary>
        /// Log the position found in rich text box
        /// </summary>
        private void LogPositionFound()
        {
            if (rbRotary == null) return;
            if (rbRotary.IsChecked == null) return;
            bool isRotary = rbRotary.IsChecked.Value;
            bool[] arrPositionOccupied = _positionViewModel.MagazinePositions.Select(o => o.Occupied).ToArray();
            int freePlace = PositionHelper.FindFreePlace(arrPositionOccupied, isRotary, numberOfPlacesNeeded);
            string magazineType = rbLinear.IsChecked.Value ? "Linear" : "isRotary";
            if (freePlace == -1)
            {
                rtbLog.AppendText($"No position is found, Number of places needed: {numberOfPlacesNeeded}, Magazine type: {magazineType}\n");
                rtbLog.ScrollToEnd();
            }
            else
            {
                rtbLog.AppendText($"Index of position found: {freePlace}, Number of places needed: {numberOfPlacesNeeded}, Magazine type: {magazineType}\n");
                rtbLog.ScrollToEnd();
            }
        }

        /// <summary>
        /// Calculate and log the result in rich text box
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Routed event arguments</param>
        private void CmdCalculate_Click(object sender, RoutedEventArgs e)
        {
            LogPositionFound();
        }
    }
}
